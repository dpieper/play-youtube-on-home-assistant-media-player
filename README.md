# YouTube Playback Script for Home Assistant
## Intro



This script allows you to stream a YouTube video to a Chromecast device via Home Assistant, addressing a challenge faced by the creator when using Home Assistant  for playing music but getting frustrated with constantly having to store music locally. With a lot of ad-hoc music to play from YouTube, an easy-to-use solution was needed. Since the terminal on his computer is open all the time anyway, the simplest way was to create a bash command to paste the YouTube video URL and play the audio on a Chromecast speaker group. 

Besides playback from the beginning, the script also supports starting playback from a specific timestamp when run in interactive mode.


## Part 1: Setup

1. **Edit `config.json`**:
   - Add your Home Assistant IP, long-lived access token, and the target speaker.
   - The default location for `config.json` is in the same directory as the script. 
   - If you want to put `config.json` somewhere else, update the `CONFIG_FILE` variable in the script, e.g. `CONFIG_FILE="/home/daniel/.config/stream-to-homeassistant/config.json"`.

2. **Example `config.json`**:
   ```json
   {
     "HASS_URL": "http://your-home-assistant:8123",
     "HASS_TOKEN": "your_long_lived_access_token",
     "CHROMECAST_ENTITY": "media_player.all_speakers",
     "DEBUG": true
   }

## Part 2: Create a Symlink

To make it easier to call the script from anywhere, you can create a symbolic link called `stream`.

1. **Create the Symlink**:
   - Run the following command to create a symlink in a directory that is in your PATH (e.g., `/usr/local/bin`):

     ```bash
     sudo ln -s /path/to/your/stream-to-home-assistant.sh /usr/local/bin/stream
     ```

   Replace `/path/to/your/stream-to-home-assistant.sh` with the actual path to your `stream-to-home-assistant.sh` script.

2. **Verify the Symlink**:
   - You can verify that the symlink works by running:

     ```bash
     stream
     ```

   This should execute the `stream-to-home-assistant.sh` script from anywhere in your terminal.

## Part 3: Usage

You can run the script either by using `./stream-to-home-assistant.sh` directly or via the symlink `stream`.

### Running the Script

1. **Without Parameters (interactive mode)**:
   - Running the script without parameters will prompt you to enter a YouTube URL, e.g. https://youtu.be/GrJs8aLfA94?si=ElJEcLO13LSZ85Uw:
     ```bash
     stream
     ```
   - This mode supports timestamp jumps, allowing you to start playback from a specific point in the video, e.g. https://youtu.be/GrJs8aLfA94?si=ElJEcLO13LSZ85Uw&t=991

2. **With Parameters**:
   - Running the script with a YouTube URL as a parameter:
     ```bash
     stream "https://www.youtube.com/watch?v=your_video_id"
     ```
   - Note: Timestamp jump mode does not work in this mode due to the limitations of handling `&` parameters in URLs in bash scripts.


## How Does This Script Work

This script was created to solve a specific challenge: the need to play ad-hoc YouTube music on Chromecast speakers via Home Assistant without the hassle of storing music locally. Here's a detailed breakdown of the script's logic and the thinking behind its development:

### 1. Configuration Loading

- **Purpose**: The script needs to communicate with your Home Assistant instance to control the media player. For this, it requires specific configuration details like the Home Assistant URL, a long-lived access token, and the target media player entity (e.g., your Chromecast speakers).
- **Implementation**:
  - The script reads these details from a `config.json` file located in the same directory as the script by default. This can be changed by updating the `CONFIG_FILE` variable.
  - The configuration file (`config.json`) includes the following fields:
    ```json
    {
      "HASS_URL": "http://your-home-assistant:8123",
      "HASS_TOKEN": "your_long_lived_access_token",
      "CHROMECAST_ENTITY": "media_player.all_speakers",
      "DEBUG": true
    }
    ```
  - The script checks if the configuration file exists and reads the necessary details. If any of these details are missing, the script exits with an error message.

### 2. URL Handling

- **Purpose**: To determine whether the script should prompt the user for a YouTube URL or use a URL provided as a command-line parameter.
- **Implementation**:
  - If no parameter is passed, the script prompts the user to enter a YouTube URL.
  - If a parameter is passed, it uses that as the YouTube URL.
  - This dual functionality allows flexibility in how the script is used. You can either interactively input a URL or automate the process by passing the URL directly.

### 3. JSON Payload Creation

- **Purpose**: To prepare the data needed to instruct Home Assistant to play the YouTube video and optionally seek to a specific timestamp.
- **Implementation**:
  - The script extracts the base URL and the timestamp (if any) from the YouTube URL.
  - A JSON payload is created for the `media_extractor.play_media` service in Home Assistant, specifying the media content type, the base URL of the YouTube video, and the target media player entity.
  - Example JSON payload:
    ```json
    {
      "media_content_type": "MUSIC",
      "media_content_id": "https://www.youtube.com/watch?v=your_video_id",
      "entity_id": "media_player.all_speakers"
    }
    ```

### 4. Sending the Playback Command

- **Purpose**: To instruct Home Assistant to start playing the specified YouTube video on the target media player.
- **Implementation**:
  - The script sends the JSON payload to Home Assistant using a POST request to the `media_extractor.play_media` service.
  - The script checks the HTTP response code to ensure the command was successfully processed by Home Assistant.
  - Debug information, including the payload and response, is optionally printed if the `DEBUG` mode is enabled.

### 5. Handling Timestamp Jumps

- **Purpose**: To allow playback to start from a specific timestamp within the YouTube video.
- **Implementation**:
  - If a timestamp is provided and the script is running in interactive mode, the script waits a few seconds to ensure playback starts, then sends a `media_player.media_seek` command to Home Assistant to jump to the specified timestamp.
  - Another JSON payload is created for the `media_player.media_seek` service, specifying the target media player entity and the seek position (timestamp).
  - Example JSON seek payload:
    ```json
    {
      "entity_id": "media_player.all_speakers",
      "seek_position": 120
    }
    ```
  - The script sends this payload to Home Assistant and checks the response to ensure the seek command was successful.
  - Debug information, including the payload and response, is optionally printed if the `DEBUG` mode is enabled.


