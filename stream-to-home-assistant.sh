#!/bin/bash

# Get the directory of the script (in case that config.json is in the same folder)
SCRIPT_DIR=$(dirname "$0")
CONFIG_FILE="$SCRIPT_DIR/config.json"

# Path to the configuration file (if outside of script directory)
CONFIG_FILE="/home/daniel/.config/stream-to-homeassistant/config.json"



#########################################################################################################################



# Check if the configuration file exists
if [ ! -f "$CONFIG_FILE" ]; then
  echo "Configuration file not found: $CONFIG_FILE"
  exit 1
fi

# Read the configuration from the JSON file
HASS_URL=$(grep -oP '(?<="HASS_URL": ")[^"]*' "$CONFIG_FILE")
HASS_TOKEN=$(grep -oP '(?<="HASS_TOKEN": ")[^"]*' "$CONFIG_FILE")
CHROMECAST_ENTITY=$(grep -oP '(?<="CHROMECAST_ENTITY": ")[^"]*' "$CONFIG_FILE")

# Check if the configuration was read successfully
if [ -z "$HASS_URL" ] || [ -z "$HASS_TOKEN" ] || [ -z "$CHROMECAST_ENTITY" ]; then
  echo "Error reading configuration. Please check $CONFIG_FILE."
  exit 1
fi

#
# Play media section
#


# Check if a YouTube URL was passed as an argument
if [ -z "$1" ]; then
  # Prompt the user for the YouTube URL if not provided as an argument
  read -p "Enter the YouTube URL: " YOUTUBE_URL
else
  # Use the provided argument as the YouTube URL
  YOUTUBE_URL="$1"
fi

# Extract the base URL and the timestamp parameter
BASE_URL=$(echo "$YOUTUBE_URL" | grep -oP '^[^&?]+[^&]*')
TIMESTAMP=$(echo "$YOUTUBE_URL" | grep -oP '(?<=t=)\d+')

# JSON payload for the media_extractor.play_media service
JSON_PAYLOAD=$(cat <<EOF
{
  "media_content_type": "MUSIC",
  "media_content_id": "$BASE_URL",
  "entity_id": "$CHROMECAST_ENTITY"
}
EOF
)

# Debugging information
if [ "$DEBUG" = true ]; then
  echo "HASS_URL: $HASS_URL"
  echo "HASS_TOKEN: ${HASS_TOKEN:0:10}..."
  echo "CHROMECAST_ENTITY: $CHROMECAST_ENTITY"
  echo "BASE_URL: $BASE_URL"
  echo "TIMESTAMP: $TIMESTAMP"
  echo "JSON_PAYLOAD: $JSON_PAYLOAD"
fi

# Send the service call to Home Assistant to start playback
RESPONSE=$(curl -s -w "\nHTTP_STATUS_CODE: %{http_code}" -X POST -H "Authorization: Bearer $HASS_TOKEN" \
     -H "Content-Type: application/json" \
     -d "$JSON_PAYLOAD" \
     "$HASS_URL/api/services/media_extractor/play_media")

# Check the HTTP response code
HTTP_STATUS_CODE=$(echo "$RESPONSE" | grep -oP 'HTTP_STATUS_CODE: \d+' | grep -oP '\d+')

# Debugging information
if [ "$DEBUG" = true ]; then
  echo "RESPONSE: $RESPONSE"
fi

if [ "$HTTP_STATUS_CODE" -eq 200 ]; then
  echo -e "\nPlayback command sent successfully.\n\n"
else
  echo "Failed to send playback command. HTTP response code: $HTTP_STATUS_CODE"
  exit 1
fi



#
# Seek media position (if applicable)
#





# If a timestamp was provided, send a seek command
if [ -n "$TIMESTAMP" ]; then
  echo -e " -> Waiting for seek command.."
  # Adding a small delay to ensure playback starts
  sleep 5
  
  JSON_SEEK_PAYLOAD=$(cat <<EOF
{
  "entity_id": "$CHROMECAST_ENTITY",
  "seek_position": $TIMESTAMP
}
EOF
  )

  # Debugging information
  if [ "$DEBUG" = true ]; then
    echo "JSON_SEEK_PAYLOAD: $JSON_SEEK_PAYLOAD"
  fi

  # Send the seek command to Home Assistant
  RESPONSE=$(curl -s -w "\nHTTP_STATUS_CODE: %{http_code}" -X POST -H "Authorization: Bearer $HASS_TOKEN" \
       -H "Content-Type: application/json" \
       -d "$JSON_SEEK_PAYLOAD" \
       "$HASS_URL/api/services/media_player/media_seek")

  # Check the HTTP response code
  HTTP_STATUS_CODE=$(echo "$RESPONSE" | grep -oP 'HTTP_STATUS_CODE: \d+' | grep -oP '\d+')

  # Debugging information
  if [ "$DEBUG" = true ]; then
    echo "RESPONSE: $RESPONSE"
  fi

  if [ "$HTTP_STATUS_CODE" -eq 200 ]; then
    echo "Seek command sent successfully."
  else
    echo "Failed to send seek command. HTTP response code: $HTTP_STATUS_CODE"
    exit 1
  fi
else
  echo -e "Note: You can provide a Youtube URL with timestamp (e.g. https://youtu.be/GrJs8aLfA94?si=YZeS4le_OYf9EB6x&t=991), however, in that case you have to call the script without parameter and use the URL prompt.\n\n"   
fi







#
# Store Media information in helper variables for internal usages (possibly not needed for others)


# Additional API call to store the YouTube URL in the helper variable
JSON_HELPER_PAYLOAD=$(cat <<EOF
{
  "entity_id": "input_text.media_inbound_stream_content_id",
  "value": "$BASE_URL"
}
EOF
)

# Send the service call to Home Assistant to update the helper variable
RESPONSE=$(curl -s -w "\nHTTP_STATUS_CODE: %{http_code}" -X POST -H "Authorization: Bearer $HASS_TOKEN" \
     -H "Content-Type: application/json" \
     -d "$JSON_HELPER_PAYLOAD" \
     "$HASS_URL/api/services/input_text/set_value")

# Check the HTTP response code
HTTP_STATUS_CODE=$(echo "$RESPONSE" | grep -oP 'HTTP_STATUS_CODE: \d+' | grep -oP '\d+')

if [ "$HTTP_STATUS_CODE" -eq 200 ]; then
  echo -e "Helper variable updated successfully.\n\n"
else
  echo "Failed to update helper variable. HTTP response code: $HTTP_STATUS_CODE"
  exit 1
fi


# Extract the title of the YouTube video using yt-dlp
VIDEO_TITLE=$(yt-dlp --get-title "$YOUTUBE_URL")

# Additional API call to store the YouTube video title in the helper variable
JSON_TITLE_PAYLOAD=$(cat <<EOF
{
  "entity_id": "input_text.media_content_title",
  "value": "$VIDEO_TITLE"
}
EOF
)

# Send the service call to Home Assistant to update the helper variable with the title
RESPONSE=$(curl -s -w "\nHTTP_STATUS_CODE: %{http_code}" -X POST -H "Authorization: Bearer $HASS_TOKEN" \
     -H "Content-Type: application/json" \
     -d "$JSON_TITLE_PAYLOAD" \
     "$HASS_URL/api/services/input_text/set_value")

# Check the HTTP response code
HTTP_STATUS_CODE=$(echo "$RESPONSE" | grep -oP 'HTTP_STATUS_CODE: \d+' | grep -oP '\d+')

if [ "$HTTP_STATUS_CODE" -eq 200 ]; then
  echo -e "Helper variable for video title updated successfully.\n\n"
else
  echo "Failed to update helper variable for video title. HTTP response code: $HTTP_STATUS_CODE"
  exit 1
fi



#